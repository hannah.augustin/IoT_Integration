# GEISt: Global Environment Information System  
<br/>
This is a project being developed for the SDI IP course at the University of Salzburg as part of the M.Sc Applied Geoinformatics program.
<br/><br/>
_Accomplices: E. Westermeier, A. Lang, h.Augustin_
<br/><br/>
Within the Salzburg University domain, GEISt can be accessed under: http://edusvr219.geo.sbg.ac.at/
<br/><br/>

## create_db.py
This script creates a PostgreSQL/PostGIS database for GEISt using Python.
<br/><br/>
## ftp.py
This script accesses and downloads surface forecasts of 6 atmospheric parameters at 4 forecast time-steps as grib files from the CAMS FTP server using authorization data located in an external text file not located in this repository.<br/><br/>
These files are then all loaded into the GEISt database as pgrasters including columns for file name, original file format (i.e. GRIB), forecast base date, forecast base time, forecast step in hours after base time (i.e. 000, 003, 006, 009), and a metadata column. The metadata column is of type xml and contains current metadata offered by CAMS for the given parameter with an updated timestamp and date. Since this metadata is streamed directly from CAMS, any changes they make to the metadata will also be reflected in the database. A copy of the metadata is written to a static location in Tomcat (i.e. ~\webapps\ROOT\metadata) under the parameter name so that it is also accessible to GEISt users.<br/><br/>
GRIB forecasts at step 000 are converted to a GeoTiff using GDAL and warped such that the prime meridian is located in the middle of the GeoTiff rather than on the left edge of the image. These GeoTiffs are saved directly in GeoServer and offered as a WMS and WCS service.<br/><br/>
## Team_Awesome.html
GEISt offers a visualization of the WMS services it offers using Cesium. This code creates the user interface in Cesium.
<br/><br/>
#### Note:
See __server_setup.md__ for what we installed on the empty server we were given to work with.
<br/><br/>