#------------------------------------------------------------------------------#
# Name:      Create PostGIS Database
#
# Authors:   E. Westermeier, A. Lang, h.Augustin
# Contact:   hannah.augustin@gmail.com
#
# Purpose:   Created for the SDI IP course for the M.Sc program in Applied
#            Geoinformatics at the University of Salzburg.
#
# Updated:   09.01.2017
#------------------------------------------------------------------------------#

'''This script is intended to be run once, creating the PostGIS Database
   for the GEISt project archive.'''

#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-

import os
import sys
import fnmatch
import subprocess
from datetime import datetime, timedelta

import psycopg2
from psycopg2.extensions import AsIs

def createPostgisDB(dbname, schema, table_names):

    '''This function creates a postgres database and a schema within it it,
        including multiple identical table structures named after the input
        list of table names.'''

    #
    # Set version of GDAL used by postgres in environment variables.
    #
    os.environ['GDAL_DATA'] = 'C:\\Program Files\\PostgreSQL\\9.6\\gdal-data'

    #
    # Connect to Postgres.
    #
    conn = None
    conn = psycopg2.connect(database='postgres',
                            user='postgres',
                            host='localhost',
                            password='geoserver',
                            port='5432')

    #
    # Create cursor for executing commands.
    #
    cur = None
    cur = conn.cursor()

    #
    # Create database. Delete database of the same name if already there.
    #
    conn.set_isolation_level(0)  # Necessary to allow database dropping.
    cur.execute('DROP DATABASE IF EXISTS %s;', (AsIs(dbname),))
    cur.execute('CREATE DATABASE %s;', (AsIs(dbname),))

    #
    # Close connection to the default database.
    #
    cur.close()
    conn.close()

    #
    # Connect to the newly created database.
    #
    conn = psycopg2.connect(database=dbname,
                            user='postgres',
                            host='localhost',
                            password='geoserver',
                            port='5432')

    #
    # Create cursor for executing commands.
    #
    cur = conn.cursor()

    #
    # Enable postgis in the new database.
    #
    cur.execute('CREATE EXTENSION postgis;')

    #
    # Create the new schema, and commit it.
    # Note: Don't forget to explicitly commit after each transaction.
    #
    cur.execute('CREATE SCHEMA %s;', (AsIs(schema),))
    conn.commit()

    #
    # Create tables based on input parameters.
    #
    for param in table_names:

        cur.execute('''CREATE TABLE
                        %s.%s (rid serial,
                                rast raster,
                                name character varying,
                                format varchar(10),
                                basedate date,
                                basetime time,
                                step char(3),
                                metadata xml);''',
                        (AsIs(schema), AsIs(param),))
    conn.commit()

    #
    # Close connection.
    #
    cur.close()
    conn.close()


if __name__ == '__main__':

    #
    # Create list of desired table names (i.e. CAMS parameters).
    #
    CAMSparams = ['gtco3', 'tc_ch4', 'tc_hno3', 'tcco', 'tcno2', 'tcso2']

    # Create database for the project, with tables in defined schema.
    createPostgisDB('geist', 'iot', CAMSparams)
