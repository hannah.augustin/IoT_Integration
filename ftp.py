#------------------------------------------------------------------------------#
# Name:      CAMS FTP to PostGIS
#
# Authors:   E. Westermeier, A. Lang, h.Augustin
# Contact:   hannah.augustin@gmail.com
#
# Purpose:   Created for the SDI IP course for the M.Sc program in Applied
#            Geoinformatics at the University of Salzburg.
#
# Updated:   16.01.2016
#------------------------------------------------------------------------------#

'''This script is intended to be run at 10:01 and 22:01 every day.
If the time is less than 10:00 on a given day, it takes the previous day
to define the folder path. Once having downloaded the desired rasters,
the data is imported into a postgis database and the local
files are removed.'''

#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-

import os
import sys
import gdal
import fnmatch
import subprocess
from ftplib import FTP
import xml.etree.ElementTree as etree
from datetime import datetime, timedelta

import requests
import psycopg2
from psycopg2.extensions import AsIs


#------------------------------------------------------------------------------#
#                             Define Functions                                 #
#------------------------------------------------------------------------------#

def getTime():

    '''This function returns the appropriate folder time (00 or 12)
        depending on current time.'''

    #
    # Get current hour in 24 format.
    #
    time_now = datetime.strftime(datetime.now(), '%H')

    #
    # Return proper result based on current time.
    #
    if 10 <= int(time_now) < 22:  # 00 forcasts released by 10:00

        #
        # This saves current time and folder time in a tuple.
        #
        goal_time = time_now, '00'

        return goal_time

    else:
        goal_time = time_now, '12'

        return goal_time


def getFolderDay():

    '''This function returns the current year, month and day,
        unless before 10:00'''

    #
    # If the current time is before 10:00, return yesterday's date.
    #
    if int(getTime()[0]) < 10:
        yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y%m%d')

        return yesterday

    else:
        today = datetime.strftime(datetime.now(), '%Y%m%d')

        return today


def connect(pass_file):

    '''This function connects to the CAMS FTP server using authorization
        credentials in a .txt file located where the python script is.
        It then returns an FTP object and a list of documents.'''

    #
    # Piece together the most recent folder name and path. The second value
    # in the tuple is the folder time (i.e. 00 or 12).
    #
    folder = getFolderDay() + getTime()[1]
    folder_path = '/DATA/CAMS_NREALTIME/{}/'.format(folder)

    #
    # Retrieve user name and pass from external text file.
    #
    try:
        with file(pass_file) as f:
            (account, passwd) = f.readline().split(' ')
            if passwd.endswith('\n'):
                passwd=passwd[:-1]
    except:
        print 'Error with password file.'
        sys.exit(-2)

    #
    # Login to the FTP server.
    #
    ftp = FTP('dissemination.ecmwf.int')
    ftp.login(user=account, passwd=passwd)

    #
    # Set current working directory and retrieve contents as a list.
    #
    ftp.cwd(folder_path)
    docs = ftp.nlst(folder_path)

    return ftp, docs


def downloadGRIB(dir_name, ftp, docs, parameter):

    '''This function is able to download all surface, forecast .grib files
       from 000, 003, 006 and 009 of a defined parameter, if so desired.'''

    #
    # Create download directory, if not already there.
    #
    if not(os.path.exists(dir_name)):
        os.mkdir(dir_name)

    #
    # Search through the list and download only the matching grib files.
    #
    for entry in docs:
        if (fnmatch.fnmatch(entry, '*_sfc_*')
                and fnmatch.fnmatch(entry, '*_fc_*')
                and (fnmatch.fnmatch(entry, '*_000_*')
                or fnmatch.fnmatch(entry, '*_003_*')
                or fnmatch.fnmatch(entry, '*_006_*')
                or fnmatch.fnmatch(entry, '*_009_*')
                )
                and fnmatch.fnmatch(entry, ('*_{}.grib').format(parameter))):
            try:

                #
                # Define path to save the file to
                #
                save_path = os.path.join(dir_name, entry)

                #
                # Open the location to save the file, then write the file.
                #
                with open(save_path, 'wb') as lf:
                    ftp.retrbinary('RETR ' + entry, lf.write, 8*1024)

                print 'Downloaded {}'.format(entry)

            except:
                print '\nError with downloading {}\n'.format(entry)
                sys.exit(-2)


def youCanQuoteMe(item):

    #
    # Solves a handfull of quotation problems, especiallz for the command line.
    #
    return "\"" + item + "\""


def translate(root_dir, grib_path, tif_path):

    ''' This function takes one GDAL input raster file (in this case GRIB)
        and translates it to GeoTIFF, then warps it to center around the
        prime meridan.'''

    root, tif = os.path.split(tif_path)

    #
    # Constants for GDAL translate, creating unwarped tifs (0-360 latitude).
    #
    gdalTranslate = 'C:\\Program Files\\GDAL\\gdal_translate.exe'
    src = grib_path
    dst = os.path.join(root_dir, tif)
    cmd = '-ot Float64 -of GTiff -a_srs EPSG:4326'

    #
    # Create GDAL translate command.
    #
    fullCmd = ' '.join([gdalTranslate, cmd, youCanQuoteMe(src),
        youCanQuoteMe(dst)])

    #
    # Execute translate and wait to finish. GeoTiffs are saved in C:\\GEIST.
    #
    myprocess = subprocess.Popen(fullCmd)
    myprocess.wait()

    #
    # Warp translated geotifs. SOURCE_EXTRA makes sure there is no gap at the
    # meridan, and CENTER_LONG makes the prime meridan the center of the image.
    #
    gdalWarp = 'C:\\Program Files\\GDAL\\gdalwarp.exe'
    src = dst
    dst = tif_path
    cmd_pt1 = '-t_srs EPSG:4326'
    cmd_pt2 = '-wo SOURCE_EXTRA=1000 --config CENTER_LONG 0'

    #
    # Create command for GDAL warp.
    #
    fullCmd = ' '.join([gdalWarp, cmd_pt1, youCanQuoteMe(src),
        youCanQuoteMe(dst), cmd_pt2])

    #
    # Execute and wait to finish, saving
    #
    myprocess = subprocess.Popen(fullCmd)
    myprocess.wait()

    #
    # Remove temporary tif file.
    #
    os.remove(src)


def createTifs(root_dir):

    ''' This function creates a GeoTiff in Geoserver, located in Tomcat.'''

    #
    # Set GDAL environment variable for translating grib to tiff.
    #
    os.environ['GDAL_DATA'] = 'C:\\Program Files\\GDAL\\gdal-data'

    #
    # Cycle through downloaded files and create GeoTIFF copies of 000 files.
    #
    for dirpath, dirnames, filenames in os.walk(root_dir, topdown=True):
        for filename in filenames:
            if filename.endswith('.grib') and fnmatch.fnmatch(filename, '*_000_*'):

                #
                # Get full path to grib file.
                #
                grib_path = os.path.join(dirpath, filename)

                #
                # Return the file name without the format.
                #
                name, file_format = filename.split('.')

                #
                # Divide the name into components to create table name.
                #
                name_parts = name.split('_')

                #
                # Creates table names, adding 'tc' to paramter if necessary.
                #
                parameter = name_parts[-1]
                if name_parts[-2] == 'tc':
                    parameter = 'tc_{}'.format(name_parts[-1])

                #
                # Translate to folder with layers set in geoserver
                # Note: admin privileges required to save in tomcat!
                #
                tif_folder = ('C:\\Program Files (x86)'
                    '\\apache-tomcat-8.5.9-windows-x86\\apache-tomcat-8.5.9'
                    '\\webapps\\geoserver\\data\\coverages\\geist')
                tif_filename = '{}.tif'.format(parameter)
                tif_path = os.path.join(tif_folder, tif_filename)

                translate(root_dir, grib_path, tif_path)


def createXml(parameter, dateinfo, timeinfo, step):

    '''This funtion creates an updated xml version for the defined CAMS
        parameter.'''

    #
    # Define proper link to xml file based on parameter.
    #
    xml_id = None

    if parameter == 'gtco3':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:o3:pid129'

    if parameter == 'tc_ch4':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:ch4:pid380'

    if parameter == 'tc_hno3':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:hno3:pid447'

    if parameter == 'tcco':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:co:pid130'

    if parameter == 'tcno2':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:no2:pid131'

    if parameter == 'tcso2':
        xml_id = 'urn:x-wmo:md:int.ecmwf::copernicus:cams:prod:fc:so2:pid134'

    if not xml_id:

        return

    root_link = ('http://apps.ecmwf.int/csw/?callback=JSON_CALLBACK&service='
        'CSW&version=2.0.2&request=GetRepositoryItem&Id=')

    xml_link = '{}{}'.format(root_link, xml_id)

    #
    # Get xml file offered by CAMS.
    #
    session = requests.Session()
    response = session.get(xml_link, stream=True)
    root = etree.fromstring(response.content)

    #
    # Search for timestamp and change to current date and time.
    #
    current_TS = root.find('{http://www.isotc211.org/2005/gmd}dateStamp')
    (current_TS[0]).text = '{:%Y-%m-%dT%H:%M:%SZ}'.format(datetime.now())
    (current_TS[0]).set('updated', 'yes')

    #
    # Search for dataset's date. Set to that of the dataset.
    #
    fc_date = root.findall('.//*{http://www.isotc211.org/2005/gco}Date')
    (fc_date[0]).text = dateinfo
    (fc_date[0]).set('updated', 'yes')

    #
    # Update XML file for GeoServer.
    #
    geoserver_folder = ('C:\\Program Files (x86)'
        '\\apache-tomcat-8.5.9-windows-x86\\apache-tomcat-8.5.9'
        '\\webapps\\ROOT\\metadata')

    xml_fn = '{}.xml'.format(parameter)

    xml_path = os.path.join(geoserver_folder, xml_fn)

    #
    # If the file already exists (as it should), delete everything and write.
    #
    if os.path.isfile(xml_path):
        with open(xml_path, "r+") as meta:
            meta.seek(0)
            meta.write(etree.tostring(root))
            meta.truncate()
    #
    # If file does not exist (e.g. first time), create file and write.
    #
    else:
        with open(xml_path, "w") as meta:
            meta.write(etree.tostring(root))

    return etree.tostring(root)


def loadPgsql(dbase, schema, root_dir):

    '''This function connects to a database, loading each grib using GDAL
        and updating the columns.'''

    #
    # Set environment variables for postgres.
    #
    os.environ['PATH'] = ';C:\\Program Files\\PostgreSQL\\9.6\\bin'
    os.environ['PGHOST'] = 'localhost'
    os.environ['PGPORT'] = '5432'
    os.environ['PGUSER'] = 'postgres'
    os.environ['PGPASSWORD'] = 'geoserver'
    os.environ['PGDATABASE'] = dbase
    os.environ['GDAL_DATA'] = 'C:\\Program Files\\PostgreSQL\\9.6\\gdal-data'

    #
    # Connect to the new database and create cursor for executing commands.
    #
    conn = psycopg2.connect(database=dbase)
    cur = conn.cursor()

    #
    # Cycle through all of the GRIB files in the folder, load them into the
    # database, and then remove the original files.
    #
    for raster in os.listdir(root_dir):

        if raster.endswith('.grib'):

            #
            # Path to grib file.
            #
            raster_path = os.path.join(root_dir, raster)

            #
            # Separate filename and format.
            #
            name, file_format = raster.split('.')

            #
            # Divide the name into components.
            #
            name_parts = name.split('_')
            datetime_part = name_parts[4]  #yyyymmddhhmmss
            year = datetime_part[:4]
            month = datetime_part[4:6]
            day = datetime_part[6:8]
            hour = datetime_part[8:10]
            minute = datetime_part[10:12]
            second = datetime_part[12:14]

            #
            # Creates table names, adding 'tc' to paramter if necessary.
            #
            parameter = name_parts[-1]
            if name_parts[-2] == 'tc':
                parameter = 'tc_{}'.format(name_parts[-1])
            table_name = '{}.{}'.format(schema, parameter)

            #
            # Forecast step time in hours.
            #
            step = name_parts[8]

            #
            # Create parts to fit postgres date and time data types.
            #
            base_date = '-'.join([year, month, day])
            base_time = ':'.join([hour, minute, second + '.00'])

            #
            # Load raster into postgres.
            #
            load = 'raster2pgsql -a -C -I -M -s 4326 {} {}.{} | psql'.format(
                raster_path, schema, parameter)
            subprocess.call(load, shell=True)
                # -a : Append raster(s) to an existing table.
                # -C : Apply raster constraints -- srid, pixelsize etc. to ensure
                     # raster is properly registered in raster_columns view
                # -I : Create a GiST index on the raster column.
                # -M : Vacuum analyze the raster table.
                # -s : Assign output raster with specified SRID.
                # See: http://postgis.net/docs/manual-2.2/using_raster_dataman.html

            #
            # Create xml file.
            #
            xml_file = None
            xml_file = createXml(parameter, base_date, base_time, step)


            #
            # Update newly created row (e.g. where basedate is empty).
            #
            cur.execute('''UPDATE %s
                            SET
                                name=(%s),
                                format=(%s),
                                basedate=(%s),
                                basetime=(%s),
                                step=(%s),
                                metadata=( XMLPARSE ( DOCUMENT %s ))
                            WHERE
                                basedate IS NULL;''',
                        (AsIs(table_name), name, file_format, base_date,
                            base_time, step, xml_file,))
            conn.commit()

            #
            # Delete duplicate rows in the case the same data are added again,
            # keeping the lowest id.
            #
            cur.execute('''DELETE FROM %s
                            USING %s tmp
                            WHERE %s.name = tmp.name
                            AND %s.format = tmp.format
                            AND %s.rid > tmp.rid;''',
                            (AsIs(table_name), AsIs(table_name), AsIs(table_name),
                                AsIs(table_name), AsIs(table_name),))
            conn.commit()

            #
            # Delete the grib file in the folder since it's now in the database.
            #
            remove = 'del {}'.format(raster_path)
            subprocess.call(remove, shell=True)

    #
    # Close the connection to the database.
    #
    cur.close()
    conn.close()


if __name__ == '__main__':

    #
    # Identify root directory to save downloads.
    #
    root_dir = 'C:\\GEIST\\'

    #
    # Connect to FTP.
    #
    ftp, docs = connect('CAMS.txt')

    #
    # List of parameters to download.
    #
    CAMS_params = [
                    'gtco3',
                    'tc_ch4',
                    'tc_hno3',
                    'tcco',
                    'tcno2',
                    'tcso2'
                    ]

    #
    # Download listed parameters for 000, 003, 006, and 009 forecast steps.
    #
    for CAMS_param in CAMS_params:

        downloadGRIB(root_dir, ftp, docs, CAMS_param)

    #
    # Disconnect from FTP.
    #
    ftp.quit()

    #
    # Create GeoTiffs from 000 time step grib files, adjust to center the
    # prime meridian and save in /data/coverages/geist folder in geoserver.
    #
    createTifs(root_dir)

    #
    # Create xml and load into postgres database.
    #
    loadPgsql('geist', 'iot', root_dir)


#------------------------------------------------------------------------------#
#                  Select references fur further development.                  #
#
# http://remote-sensing.eu/managing-raster-data-with-postgis-and-python/
# http://gis.stackexchange.com/questions/62026/how-to-import-geotiff-via-postgis-into-geoserver
