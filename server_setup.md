1. Turn off IE security settings  
2. Download and install Firefox  
3. Download and install 7-Zip: http://www.7-zip.org/  
4. Download and install Notepad ++: https://notepad-plus-plus.org/  
5. Download and install Java 8.0: https://java.com/en/download/manual.jsp  
6. Download and install Server JRE 8u111: http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html  
7. Download Apache Tomcat 8: http://tomcat.apache.org/download-80.cgi
  (Instructions: http://tomcat.apache.org/tomcat-8.5-doc/RUNNING.txt)  
 * Change system environment variables (CATALINA_HOME, JRE_HOME, JAVA_BASE)  
8. Download and install Geoserver Stable 2.10: http://geoserver.org/release/2.10.0/
  (Instructions: http://geoserver.geo-solutions.it/edu/en/install_run/gs_install.html)
 * Put Geoserver .war file in the webapps folder of Tomcat  
 * Access via localhost:8080\geoserver  
 (Instructions: http://docs.geoserver.org/latest/en/user/gettingstarted/web-admin-quickstart/index.html)  
9. Download and install Postgresql: https://www.postgresql.org/download/
 * Follow installer  
 * Stack Builder -- install PostGIS  
10. Download and Install Python 2.7.12
 * Change system environment path variables to Python 2.7 folder
 * Change system environment path variable to Scripts folder in Python27 folder
11. Install psycopg2 using pip
 * pip install psycopg2
12. Install requests python library for HTTP requests of xml files 
 * pip install requests (see: http://docs.python-requests.org)
13. Install git for Windows
 * https://git-scm.com/download/win
 * Install GitHub Desktop: https://desktop.github.com/
 * Clone repository
14. Download and install atom text editor
 * https://atom.io/
15. Install JDBC Image Mosaic Extension on Geoserver
 * http://geoserver.org/release/2.10.x/
16. Install CSW plugin
 * http://docs.geoserver.org/2.7.6/user/extensions/csw/installing.html
17. Install Cesium.js V1.28
 * https://cesiumjs.org/downloads.html
18. Install Node.js LTS v6.9.2 (.msi -> .exe was not working)
 * https://nodejs.org/en/download/
 * Localhost:8006 (Tipp: finding localhost over netstat -a -b in cmd)
19. Install Microsoft Visual C++ 2008
 * http://www.lfd.uci.edu/~gohlke/pythonlibs/
20. Download and install GDAL for Windows
 * Go to: http://www.gisinternals.com/release.php
 * Python 2.7 uses 1500 which is MSVC 2008 -- find the correct GDAL core installer
    * e.g. gdal-201-1500-x64-core.msi
 * Install using the installer
 * Set system path environment variable to include GDAL folder
    * e.g. C:\Program Files\GDAL
 * Set system environment variable GDAL_DATA to the gdal-data folder in GDAL
    * e.g. C:\Program Files\GDAL\gdal-data
 * Set system environment variable GDAL_DRIVER_PATH to gdalplugins folder
    * e.g. C:\Program Files\GDAL\gdalplugins
 * Note: No Python bindings required, since called using "subprocess" module
21. Schedule ftp script to run at 10 and 22
 * Task Scheduler and create a task to run at 10:00 and 22:00
 * Set Powershell as program, with python path and script path as parameters.

22. GeoServer setup for Tifs (http://wiki.ieee-earth.org/index.php?title=Documents/GEOSS_Tutorials/GEOSS_Provider_Tutorials/Web_Coverage_Service_Tutorial_for_GEOSS_Providers/Section_4_:_Provisioning%2F%2FUsing_the_service_or_application/Section_4.3_:_Detailed_steps_of_the_Use_Cases/4.3.1_Offering_a_GeoTiff_dataset_with_Geoserver)
 * create workspace "geist" in Geoserver
 * Set workspace to be default and service to be WCS and WMS
 * Create folder "geist" ('C:\Program Files (x86)\apache-tomcat-8.5.9-windows-x86\apache-tomcat-8.5.9\webapps\geoserver\data\coverages\geist')
 * Create store "geist", selecting the "geist" folder path to the desired data
 * Create layers for each parameter
 * Create a style for each of the the WMS services

23. Set Tomcat to go out
 * Open port 80 in the Windows Server Firewall settings
 * Edit port from 8080 to 80 in Tomcat config file.
 * Create Tomcat Manger User:Login info


# TODO

24. Install Apache HTTP Web Server()
 * Install Microsoft Visual C++ 2015 Redistributable Update 3 (https://www.microsoft.com/en-us/download/confirmation.aspx?id=53840)
 * Download Apache from: http://www.apachelounge.com/download/
 * 