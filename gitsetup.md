## Setting up Global Environment:
* $ git config --global user.name "[insert your name]"
* $ git config --global user.email "[insert your email]"
* $ git config --global core.editor "atom --wait"


## Clone to local
Assuming a repository (empty or full) exists on GitLab already that you want to
clone to work on locally...


## Mirror to GitHub (assuming already created in GitHub)
1. navigate to local repository
2. do a pull request to make sure it is up-to-date (*git pull*)
3. $ git remote add github http://[Github username]@github.com/[Github username]/[Github repo name].git
4. $ git push --mirror github
5. to keep the mirror up to date, do the same steps but skip step 3.

## Get help in CLI:
    $ git help <verb>
    $ git <verb> --help
    $ man git-<verb>